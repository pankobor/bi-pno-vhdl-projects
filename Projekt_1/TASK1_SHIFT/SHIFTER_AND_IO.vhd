----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Cyclic bi-directional shifter with interface
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SHIFTER_AND_IO is
   port (
      SWITCH   : in  std_logic_vector (7 downto 0);
      BTN0     : in  std_logic;
      BTN1     : in  std_logic;
      BTN2     : in  std_logic;
      BTN3     : in  std_logic;
      CLK      : in  std_logic;
      RESET    : in  std_logic;
      SEGMENT  : out std_logic_vector (6 downto 0);
      DP       : out std_logic;
      DIGIT    : out std_logic_vector (3 downto 0);
      LED_R    : out std_logic
   );
end SHIFTER_AND_IO;

architecture SHIFTER_AND_IO_BODY of SHIFTER_AND_IO is
   component CYCLIC_SHIFTER is
      port (
         INPUT             : in  std_logic_vector (7 downto 0);
         BTN0              : in  std_logic;
         BTN1              : in  std_logic;
         BTN2              : in  std_logic;
         CLK               : in  std_logic;
         RESET             : in  std_logic;
         OUTPUT            : out std_logic_vector (7 downto 0);
         COPY_A            : out std_logic_vector (7 downto 0);
         COPY_L            : out std_logic_vector (2 downto 0);
         SHIFT_DIRECTION_R : out std_logic
      );
   end component CYCLIC_SHIFTER;
   
   component HEX2SEG is
      port (
         DATA     : in  STD_LOGIC_VECTOR (15 downto 0);   -- vstupni data k zobrazeni (4 sestnactkove cislice)
         CLK      : in  STD_LOGIC;
         SEGMENT  : out STD_LOGIC_VECTOR (6 downto 0);    -- 7 segmentu displeje
         DP       : out STD_LOGIC;                        -- desetinna tecka
         DIGIT    : out STD_LOGIC_VECTOR (3 downto 0)     -- 4 cifry displeje
      );
   end component HEX2SEG;
   
   signal SHIFT_DIRECTION_R   : std_logic;
   signal COPY_L              : std_logic_vector (2 downto 0);
   signal COPY_A, OUTPUT      : std_logic_vector (7 downto 0);
   signal DATA                : std_logic_vector (15 downto 0);
   
begin

   SHIFT_INST : CYCLIC_SHIFTER port map (
      INPUT             => SWITCH,
      BTN0              => BTN0,
      BTN1              => BTN1,
      BTN2              => BTN2,
      CLK               => CLK,
      RESET             => RESET,
      OUTPUT            => OUTPUT,
      COPY_A            => COPY_A,
      COPY_L            => COPY_L,
      SHIFT_DIRECTION_R => SHIFT_DIRECTION_R
   );
   
   DISP_INST : HEX2SEG port map (
      DATA     => DATA,
      CLK      => CLK,
      SEGMENT  => SEGMENT,
      DP       => DP,
      DIGIT    => DIGIT
   );

   DISPLAY_MUX : process (BTN3, OUTPUT, COPY_A, COPY_L)
   begin
      if BTN3 = '0' then
         DATA <= "00000000" & OUTPUT; -- if btn3 is not active send the output of shifter on display
      else
         DATA <= (COPY_A & "00000" & COPY_L); -- if btn3 is active send copy of operand A with copy of operand L to display
      end if; 
   end process DISPLAY_MUX;
   
   LED_MUX : process (BTN3, SHIFT_DIRECTION_R)
   begin
      if BTN3 = '0' then
         LED_R <= '0'; -- if btn3 is not active don't light LED
      else
         LED_R <= SHIFT_DIRECTION_R; -- if btn3 is active then drive the LED if last shift was right shift
      end if;
   end process LED_MUX;

end architecture SHIFTER_AND_IO_BODY;
