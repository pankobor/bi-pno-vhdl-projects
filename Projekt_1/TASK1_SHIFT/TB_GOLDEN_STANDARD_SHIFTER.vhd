----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Testbench for cyclic bi-directional shifter
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TB_GOLDEN_STANDARD_SHIFTER is
end TB_GOLDEN_STANDARD_SHIFTER;

architecture TB_GOLDEN_STANDARD_SHIFTER_BODY of TB_GOLDEN_STANDARD_SHIFTER is

   component CYCLIC_SHIFTER is
      port (
         INPUT             : in  std_logic_vector (7 downto 0);
         BTN0              : in  std_logic;
         BTN1              : in  std_logic;
         BTN2              : in  std_logic;
         CLK               : in  std_logic;
         RESET             : in  std_logic;
         OUTPUT            : out std_logic_vector (7 downto 0);
         COPY_A            : out std_logic_vector (7 downto 0);
         COPY_L            : out std_logic_vector (2 downto 0);
         SHIFT_DIRECTION_R : out std_logic
      );
   end component CYCLIC_SHIFTER;
   
   component CYCLIC_SHIFTER_SW is
      port (
         INPUT             : in  std_logic_vector (7 downto 0);
         BTN0              : in  std_logic;
         BTN1              : in  std_logic;
         BTN2              : in  std_logic;
         CLK               : in  std_logic;
         RESET             : in  std_logic;
         OUTPUT            : out std_logic_vector (7 downto 0);
         COPY_A            : out std_logic_vector (7 downto 0);
         COPY_L            : out std_logic_vector (2 downto 0);
         SHIFT_DIRECTION_R : out std_logic
      );
   end component CYCLIC_SHIFTER_SW;
   
   signal TB_INPUT                     : std_logic_vector (7 downto 0);
   signal TB_BTN0                      : std_logic;
   signal TB_BTN1                      : std_logic;
   signal TB_BTN2                      : std_logic;
   signal TB_CLK                       : std_logic;
   signal TB_RESET                     : std_logic;
   signal TB_OUTPUT_DUT                : std_logic_vector (7 downto 0);
   signal TB_OUTPUT_GOLDEN             : std_logic_vector (7 downto 0);
   signal TB_COPY_A_DUT                : std_logic_vector (7 downto 0);
   signal TB_COPY_A_GOLDEN             : std_logic_vector (7 downto 0);
   signal TB_COPY_L_DUT                : std_logic_vector (2 downto 0);
   signal TB_COPY_L_GOLDEN             : std_logic_vector (2 downto 0);
   signal TB_SHIFT_DIRECTION_R_DUT     : std_logic;
   signal TB_SHIFT_DIRECTION_R_GOLDEN  : std_logic;
   
   constant CLK_PERIOD           : time := 10 ns;
   
begin

   DUT : CYCLIC_SHIFTER port map (
      INPUT             => TB_INPUT,
      BTN0              => TB_BTN0,
      BTN1              => TB_BTN1,
      BTN2              => TB_BTN2,
      CLK               => TB_CLK,
      RESET             => TB_RESET,
      OUTPUT            => TB_OUTPUT_DUT,
      COPY_A            => TB_COPY_A_DUT,
      COPY_L            => TB_COPY_L_DUT,
      SHIFT_DIRECTION_R => TB_SHIFT_DIRECTION_R_DUT
   );
   
   GOLDEN : CYCLIC_SHIFTER_SW port map (
         INPUT             => TB_INPUT,
         BTN0              => TB_BTN0,
         BTN1              => TB_BTN1,
         BTN2              => TB_BTN2,
         CLK               => TB_CLK,
         RESET             => TB_RESET,
         OUTPUT            => TB_OUTPUT_GOLDEN,
         COPY_A            => TB_COPY_A_GOLDEN,
         COPY_L            => TB_COPY_L_GOLDEN,
         SHIFT_DIRECTION_R => TB_SHIFT_DIRECTION_R_GOLDEN
      );
   
   CLK_GEN : process
   begin
      TB_CLK <= '0';
      wait for CLK_PERIOD/2;
      TB_CLK <= '1';
      wait for CLK_PERIOD/2;
   end process;
   
   RESET_GEN : process
   begin
      wait for 50 ns;
      TB_RESET <= '0';   
      wait for 50 ns;
      TB_RESET <= '1';   
      wait for 50 ns;
      TB_RESET <= '0';
      wait;
   end process; 
   
   STIMULI_GEN : process
   begin
      wait until TB_RESET = '1';
      TB_BTN0 <= '0';   --initialize buttons
      TB_BTN1 <= '0';
      TB_BTN2 <= '0';
      wait until TB_RESET = '0';
      wait for 33 ns;

      -- loop trough all combinations of inputs A and L
      -- and for all combinations do right shift first
      -- and then left shift second with same input data
      for A in 0 to 255 loop
         for L in 0 to 7 loop
            
------------SHIFT-RIGHT--------------------------------------------
            wait until TB_CLK = '1';

            -- set input A on switches and push button
            TB_INPUT <= std_logic_vector(to_unsigned(A,8));
            wait for 2*CLK_PERIOD + 3 ns;
            TB_BTN0 <= '1';
            wait for 5*CLK_PERIOD + 3 ns;
            TB_BTN0 <= '0';

            wait for 3*CLK_PERIOD + 5 ns;

            -- set input A on switches and push button 1 for right shift
            TB_INPUT <= std_logic_vector(to_unsigned(L,8));
            wait for 2*CLK_PERIOD + 3 ns;
            TB_BTN1 <= '1';
            wait for 5*CLK_PERIOD + 3 ns;
            TB_BTN1 <= '0';

            wait for 10*CLK_PERIOD;

            -- assert output of DUT by comparing it to software model of shifter
            assert TB_OUTPUT_DUT = TB_OUTPUT_GOLDEN
               report "CHYBA: Vstupy: " & integer'image(A) & " "&integer'image(L) & ", Vystup: " & integer'image(to_integer(unsigned(TB_OUTPUT_DUT))) & "; Ocakavany: " & integer'image(to_integer(unsigned(TB_OUTPUT_GOLDEN))) 
               severity error;
               
------------SHIFT-LEFT--------------------------------------------
            wait until TB_CLK = '1';

            -- set input A on switches and push button
            TB_INPUT <= std_logic_vector(to_unsigned(A,8));
            wait for 2*CLK_PERIOD + 3 ns;
            TB_BTN0 <= '1';
            wait for 5*CLK_PERIOD + 3 ns;
            TB_BTN0 <= '0';

            wait for 3*CLK_PERIOD + 5 ns;

            -- set input A on switches and push button 1 for left shift
            TB_INPUT <= std_logic_vector(to_unsigned(L,8));
            wait for 2*CLK_PERIOD + 3 ns;
            TB_BTN2 <= '1';
            wait for 5*CLK_PERIOD + 3 ns;
            TB_BTN2 <= '0';

            wait for 10*CLK_PERIOD;

            -- assert output of DUT by comparing it to software model of shifter
            assert TB_OUTPUT_DUT = TB_OUTPUT_GOLDEN
               report "CHYBA: Vstupy: " & integer'image(A) & " "&integer'image(L) & ", Vystup: " & integer'image(to_integer(unsigned(TB_OUTPUT_DUT))) & "; Ocakavany: " & integer'image(to_integer(unsigned(TB_OUTPUT_GOLDEN))) 
               severity error;
               
         end loop;
      end loop; 
      
      report "KONIEC SIMULACIE" severity failure;
      
   end process;

end TB_GOLDEN_STANDARD_SHIFTER_BODY;
