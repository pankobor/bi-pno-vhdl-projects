----------------------------------------------------------------------------------
-- Company:    CTU in Prague, Faculty of Information Technology
-- Engineer:   Boris Pankovcin
--  
-- Description: Controller for cyclic bi-directional shifter 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CONTROLLER is
   port (
      BTN0           : in  std_logic;
      BTN1           : in  std_logic;
      BTN2           : in  std_logic;
      ZERO           : in  std_logic;
      CLK            : in  std_logic;
      RESET          : in  std_logic;
      LOAD_A         : out std_logic;
      LOAD_L         : out std_logic;
      SHIFT_R        : out std_logic;
      SHIFT_L        : out std_logic;
      EN_CNT         : out std_logic;
      RESET_DATAPATH : out std_logic
   );
end CONTROLLER;

architecture CONTROLLER_BODY of CONTROLLER is
   type TYP_STAV is (ST_WAIT_A, ST_LD_A, ST_WAIT_L, ST_LD_L_RIGHT, ST_LD_L_LEFT, ST_SHIFT_LEFT, ST_SHIFT_RIGHT);
   signal STAV, DALSI_STAV : TYP_STAV;

begin
   PRECHODY : process (STAV, BTN0, BTN1, BTN2, ZERO)
   begin
      case STAV is
         when ST_WAIT_A       => if BTN0 = '0' then
                                    DALSI_STAV <= ST_WAIT_A;
                                 else
                                    DALSI_STAV <= ST_LD_A;
                                 end if;
         when ST_LD_A         => DALSI_STAV <= ST_WAIT_L;
         when ST_WAIT_L       => if BTN1 = '1' then
                                    DALSI_STAV <= ST_LD_L_RIGHT;
                                 elsif BTN2 = '1' then
                                    DALSI_STAV <= ST_LD_L_LEFT;
                                 else
                                    DALSI_STAV <= ST_WAIT_L;
                                 end if;
         when ST_LD_L_RIGHT   => DALSI_STAV <= ST_SHIFT_RIGHT;
         when ST_LD_L_LEFT    => DALSI_STAV <= ST_SHIFT_LEFT;
         when ST_SHIFT_RIGHT  => if ZERO = '0' then
                                    DALSI_STAV <= ST_SHIFT_RIGHT;
                                 else
                                    DALSI_STAV <= ST_WAIT_A;
                                 end if;
         when ST_SHIFT_LEFT   => if ZERO = '0' then
                                    DALSI_STAV <= ST_SHIFT_LEFT;
                                 else
                                    DALSI_STAV <= ST_WAIT_A;
                                 end if;
      end case;
   end process;
   
   VYSTUPY : process (STAV)
   begin
      LOAD_A         <= '0';
      LOAD_L         <= '0';
      SHIFT_R        <= '0';
      SHIFT_L        <= '0';
      EN_CNT         <= '0';
      RESET_DATAPATH <= '0';
      case STAV is 
         when ST_WAIT_A       => null;
         when ST_LD_A         => LOAD_A         <= '1'; -- while loading operand A from the input
                                 RESET_DATAPATH <= '1'; -- it will also reset datapath to remove old data from output
         when ST_WAIT_L       => null;
         when ST_LD_L_RIGHT   => LOAD_L         <= '1';
         when ST_LD_L_LEFT    => LOAD_L         <= '1';
         when ST_SHIFT_RIGHT  => SHIFT_R        <= '1';
                                 EN_CNT         <= '1';
         when ST_SHIFT_LEFT   => SHIFT_L        <= '1';
                                 EN_CNT         <= '1';
      end case;
   end process;
   
   REG_STAVU : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if RESET = '1' then
            STAV <= ST_WAIT_A;
         else
            STAV <= DALSI_STAV;
         end if;
      end if;
   end process;

end CONTROLLER_BODY;
