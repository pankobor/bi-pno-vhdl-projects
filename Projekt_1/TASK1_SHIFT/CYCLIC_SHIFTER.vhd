----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Cyclic bi-directional shifter - RTL description and SW model
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity CYCLIC_SHIFTER is
   port (
      INPUT             : in  std_logic_vector (7 downto 0);
      BTN0              : in  std_logic;
      BTN1              : in  std_logic;
      BTN2              : in  std_logic;
      CLK               : in  std_logic;
      RESET             : in  std_logic;
      OUTPUT            : out std_logic_vector (7 downto 0);
      COPY_A            : out std_logic_vector (7 downto 0);
      COPY_L            : out std_logic_vector (2 downto 0);
      SHIFT_DIRECTION_R : out std_logic
   );
end entity CYCLIC_SHIFTER;

architecture CYCLIC_SHIFTER_BODY of CYCLIC_SHIFTER is

   component DATAPATH is
      port (
         INPUT             : in  std_logic_vector(7 downto 0);
         LOAD_A            : in  std_logic;
         LOAD_L            : in  std_logic;
         SHIFT_R           : in  std_logic;
         SHIFT_L           : in  std_logic;
         EN_CNT            : in  std_logic;
         CLK               : in  std_logic;
         RESET             : in  std_logic;
         OUTPUT            : out std_logic_vector (7 downto 0);
         COPY_A            : out std_logic_vector (7 downto 0);
         COPY_L            : out std_logic_vector (2 downto 0);
         SHIFT_DIRECTION_R : out std_logic;
         ZERO              : out std_logic
      );
   end component DATAPATH;

   component CONTROLLER is
      port (
         BTN0           : in  std_logic;
         BTN1           : in  std_logic;
         BTN2           : in  std_logic;
         ZERO           : in  std_logic;
         CLK            : in  std_logic;
         RESET          : in  std_logic;
         LOAD_A         : out std_logic;
         LOAD_L         : out std_logic;
         SHIFT_R        : out std_logic;
         SHIFT_L        : out std_logic;
         EN_CNT         : out std_logic;
         RESET_DATAPATH : out std_logic
      );
   end component CONTROLLER;

   signal LOAD_A, LOAD_L, ZERO, SHIFT_R, SHIFT_L, RESET_DATAPATH, EN_CNT : std_logic;

begin

   DATAPATH_INST : DATAPATH port map (
      INPUT             => INPUT,
      LOAD_A            => LOAD_A,
      LOAD_L            => LOAD_L,
      SHIFT_R           => SHIFT_R,
      SHIFT_L           => SHIFT_L,
      EN_CNT            => EN_CNT,
      CLK               => CLK,
      RESET             => RESET_DATAPATH,
      OUTPUT            => OUTPUT,
      COPY_A            => COPY_A,
      COPY_L            => COPY_L,
      SHIFT_DIRECTION_R => SHIFT_DIRECTION_R,
      ZERO              => ZERO
   );
   
   CONTROLLER_INST : CONTROLLER port map (
      BTN0           => BTN0,
      BTN1           => BTN1,
      BTN2           => BTN2,
      ZERO           => ZERO,
      CLK            => CLK,
      RESET          => RESET,
      LOAD_A         => LOAD_A,
      LOAD_L         => LOAD_L,
      SHIFT_R        => SHIFT_R,
      SHIFT_L        => SHIFT_L,
      EN_CNT         => EN_CNT,
      RESET_DATAPATH => RESET_DATAPATH
   );

end architecture CYCLIC_SHIFTER_BODY;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity CYCLIC_SHIFTER_SW is
   port (
      INPUT             : in  std_logic_vector (7 downto 0);
      BTN0              : in  std_logic;
      BTN1              : in  std_logic;
      BTN2              : in  std_logic;
      CLK               : in  std_logic;
      RESET             : in  std_logic;
      OUTPUT            : out std_logic_vector (7 downto 0);
      COPY_A            : out std_logic_vector (7 downto 0);
      COPY_L            : out std_logic_vector (2 downto 0);
      SHIFT_DIRECTION_R : out std_logic
   );
end entity CYCLIC_SHIFTER_SW;

architecture SOFTWARE_MODEL of CYCLIC_SHIFTER_SW is
begin

   SW_MODEL : process
      variable A : std_logic_vector (7 downto 0);
      variable L : unsigned (2 downto 0);
   begin
      -- wait for BTN0 then load A and clear output
      wait until BTN0 = '1';
      A := INPUT;
      COPY_A <= INPUT;
      OUTPUT <= "00000000";
      
      -- wait for BTN1 or BTN2 then load L and shift
      wait until BTN1 = '1' or BTN2 = '1';
      if BTN1 = '1' then
         L := unsigned(INPUT (2 downto 0)); -- load L
         COPY_L <= INPUT (2 downto 0);
         
         SHIFT_DIRECTION_R <= '1'; -- set shift direction indication for LED
         
         if L /= 0 then
            -- in SW model shift only by rearranging the std_logic_vector
            OUTPUT <= A(to_integer(L - 1) downto 0) & A(7 downto to_integer(L));
         else
            OUTPUT <= A; -- if L was 0 then no shift is needed
         end if;
      elsif BTN2 = '1' then
         L := unsigned(INPUT (2 downto 0)); -- load L
         COPY_L <= INPUT (2 downto 0);
         
         SHIFT_DIRECTION_R <= '0'; -- set shift direction indication for LED
         
         if L /= 0 then
            -- in SW model shift only by rearranging the std_logic_vector
            OUTPUT <= A(to_integer(7 - L) downto 0) & A(7 downto to_integer(8 - L));
         else
            OUTPUT <= A; -- if L was 0 then no shift is needed
         end if;
      end if;
   end process SW_MODEL;
end architecture SOFTWARE_MODEL;