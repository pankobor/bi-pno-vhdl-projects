----------------------------------------------------------------------------------
-- Company:    CTU in Prague, Faculty of Information Technology
-- Engineer:   Boris Pankovcin
--  
-- Description: Datapath for cyclic bi-directional shifter 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity DATAPATH is
   port (
      INPUT             : in  std_logic_vector(7 downto 0);
      LOAD_A            : in  std_logic;
      LOAD_L            : in  std_logic;
      SHIFT_R           : in  std_logic;
      SHIFT_L           : in  std_logic;
      EN_CNT            : in  std_logic;
      CLK               : in  std_logic;
      RESET             : in  std_logic;
      OUTPUT            : out std_logic_vector (7 downto 0);
      COPY_A            : out std_logic_vector (7 downto 0);
      COPY_L            : out std_logic_vector (2 downto 0);
      SHIFT_DIRECTION_R : out std_logic;
      ZERO              : out std_logic
   );
end DATAPATH;

architecture DATAPATH_BODY of DATAPATH is

   signal C, SHIFTED    : std_logic_vector (7 downto 0);
   signal CNT_SHIFT     : unsigned (2 downto 0);
   
begin
   
   -- shift register is responsible for storing operand A and shifting it
   -- while also it will send copy of operand A to output while loading it
   SHIFT_REG : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if LOAD_A = '1' then
            SHIFTED  <= INPUT;
            COPY_A   <= INPUT;
         elsif SHIFT_R = '1' then
            SHIFT_DIRECTION_R <= '1'; -- set shift direction indication for LED
            SHIFTED <= SHIFTED(0) & SHIFTED (7 downto 1); -- shift right by shifting whole std_logic_vector one bit right for L times
         elsif SHIFT_L = '1' then
            SHIFT_DIRECTION_R <= '0'; -- set shift direction indication for LED
            SHIFTED <= SHIFTED (6 downto 0) & SHIFTED(7); -- shift right by shifting whole std_logic_vector one bit left for L times
         end if;
      end if;
   end process;

   -- counter is responsible for storing operand L and then substract it until 0
   -- while also it will send copy of operand L to output while loading it
   COUNTER : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if LOAD_L = '1' then
            CNT_SHIFT   <= unsigned(INPUT (2 downto 0));
            COPY_L      <= INPUT (2 downto 0);
         elsif EN_CNT = '1' then
            CNT_SHIFT   <= CNT_SHIFT - 1; -- when controller allows counter substract every clock cycle by one
         end if;
      end if;
   end process;
   
   -- this process will detect that substracting of operand L finished - L reached zero
   SHIFT_FINISH : process (CNT_SHIFT)
   begin
      if CNT_SHIFT = 0 then
         ZERO <= '1';
      else
         ZERO <= '0';
      end if;
   end process;
   
   -- register C is responsible for holding data that are connected to output
   REG_C : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if RESET = '1' then
            C <= (others => '0');
         elsif SHIFT_R = '1' or SHIFT_L = '1' then
            C <= SHIFTED;
         end if;
      end if;
   end process;
   
   CIRCUIT_OUTPUT : process (C)
   begin
      OUTPUT <= C;
   end process;

end DATAPATH_BODY;
