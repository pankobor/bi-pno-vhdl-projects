----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: ROM simulating entity
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ROM is
   port (
      CLK  : in  std_logic;
      ADDR : in  std_logic_vector(7 downto 0);
      DATA : out std_logic_vector(7 downto 0)
   );
end ROM;

architecture ROM_BODY of ROM is

begin

   ROM_OUTPUT : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         DATA <= std_logic_vector(to_unsigned(255 - to_integer(unsigned(ADDR)), 8));
      end if;
   end process;

end ROM_BODY;
