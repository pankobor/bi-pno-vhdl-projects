----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: State machine for sending data or command to LCD display
--              using the 4-bit parallel interface
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity LCD_FUNCTION_STM is
   port (
      CMD_DATA    : in  std_logic_vector (7 downto 0);
      CMD_START   : in  std_logic;
      INIT_START  : in  std_logic;
      CLK         : in  std_logic;
      RESET       : in  std_logic;
      SF_D        : out std_logic_vector (11 downto 8);
      CMD_DONE    : out std_logic;
      LCD_E       : out std_logic
   );
end LCD_FUNCTION_STM;

architecture LCD_FUNCTION_STM_BODY of LCD_FUNCTION_STM is
   
   signal CNT     : unsigned (10 downto 0);
   signal CNT1    : unsigned (10 downto 0);
   signal CNT2    : unsigned (10 downto 0);
   signal EN_CNT1 : std_logic;
   signal EN_CNT2 : std_logic;
   signal LD_CNT1 : std_logic;
   signal LD_CNT2 : std_logic;
   signal ZERO1   : std_logic;
   signal ZERO2   : std_logic;
   
   type TYPE_STATE is (WAIT_CMD, WAIT_CMD_DELAY, PREDSTIH_UPPER, UPPER, PRESAH_UPPER, PREDSTIH_LOWER, LOWER, PRESAH_LOWER, CMD_END);
   signal STATE, NEXT_STATE : TYPE_STATE;
   
begin

   AUX_COUNTER1 : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if LD_CNT1 = '1' then              
            CNT1 <= CNT;
         elsif EN_CNT1 = '1' then           
            CNT1 <= CNT1 - 1;
         end if;
      end if;
   end process;
   
   CNT1_FINISH : process (CNT1)
      begin
         if CNT1 = 0 then
            ZERO1 <= '1';
         else
            ZERO1 <= '0';
         end if;
   end process;
   
   AUX_COUNTER2 : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if LD_CNT2 = '1' then              
            CNT2 <= CNT;
         elsif EN_CNT2 = '1' then           
            CNT2 <= CNT2 - 1;
         end if;
      end if;
   end process;
   
   CNT2_FINISH : process (CNT2)
      begin
         if CNT2 = 0 then
            ZERO2 <= '1';
         else
            ZERO2 <= '0';
         end if;
   end process;
   
   TRANP : process (STATE, CMD_START, INIT_START, ZERO1, ZERO2)
   begin
      case STATE is
         when WAIT_CMD        => if CMD_START = '1' or INIT_START = '1' then
                                    NEXT_STATE <= WAIT_CMD_DELAY;
                                 else
                                    NEXT_STATE <= WAIT_CMD;
                                 end if;
         when WAIT_CMD_DELAY  => if INIT_START ='1' then
                                    NEXT_STATE <= PREDSTIH_LOWER; -- when using with init cmd we need to send only lower nibble
                                 else
                                    NEXT_STATE <= PREDSTIH_UPPER;
                                 end if;
         when PREDSTIH_UPPER  => if ZERO1 = '1' then
                                    NEXT_STATE <= UPPER;
                                 else
                                    NEXT_STATE <= PREDSTIH_UPPER;
                                 end if;
         when UPPER           => if ZERO2 = '1' then
                                    NEXT_STATE <= PRESAH_UPPER;
                                 else
                                    NEXT_STATE <= UPPER;
                                 end if;
         when PRESAH_UPPER    => if ZERO1 = '1' then
                                    NEXT_STATE <= PREDSTIH_LOWER;
                                 else
                                    NEXT_STATE <= PRESAH_UPPER;
                                 end if;
         when PREDSTIH_LOWER  => if ZERO2 = '1' then
                                    NEXT_STATE <= LOWER;
                                 else
                                    NEXT_STATE <= PREDSTIH_LOWER;
                                 end if;
         when LOWER           => if ZERO1 = '1' then
                                    NEXT_STATE <= PRESAH_LOWER;
                                 else
                                    NEXT_STATE <= LOWER;
                                 end if;
         when PRESAH_LOWER    => if ZERO2 = '1' then
                                    NEXT_STATE <= CMD_END;
                                 else
                                    NEXT_STATE <= PRESAH_LOWER;
                                 end if;
         when CMD_END         => NEXT_STATE <= WAIT_CMD;
      end case;
   end process;
   
   OUTP : process (STATE)
   begin
      SF_D      <= (others => '0'); 
      LCD_E     <= '0';
      CMD_DONE  <= '0';
      EN_CNT1   <= '0';
      EN_CNT2   <= '0';
      LD_CNT1   <= '0';
      LD_CNT2   <= '0';
      CNT       <= to_unsigned(0, 11);
      case STATE is
         when WAIT_CMD        => LD_CNT1 <= '1';
                                 LD_CNT2 <= '1';
                                 CNT <= to_unsigned(2, 11);
         when CMD_END         => CMD_DONE <= '1';
         when WAIT_CMD_DELAY  => 
         when PREDSTIH_UPPER  => EN_CNT1 <= '1';
                                 LD_CNT2 <= '1';
                                 CNT <= to_unsigned(11, 11);
                                 SF_D <= CMD_DATA(7 downto 4);
         when UPPER           => EN_CNT2 <= '1';
                                 LD_CNT1 <= '1';
                                 CNT <= to_unsigned(52, 11); -- 51
                                 LCD_E <= '1';
                                 SF_D <= CMD_DATA(7 downto 4);
         when PRESAH_UPPER    => EN_CNT1 <= '1';
                                 LD_CNT2 <= '1';
                                 CNT <= to_unsigned(2, 11);
                                 SF_D <= CMD_DATA(7 downto 4);
         when PREDSTIH_LOWER  => EN_CNT2 <= '1';
                                 LD_CNT1 <= '1';
                                 CNT <= to_unsigned(11, 11);
                                 SF_D <= CMD_DATA(3 downto 0);
         when LOWER           => EN_CNT1 <= '1';
                                 LD_CNT2 <= '1';
                                 CNT <= to_unsigned(2010, 11); -- 2000
                                 LCD_E <= '1';
                                 SF_D <= CMD_DATA(3 downto 0);
         when PRESAH_LOWER    => EN_CNT2 <= '1';
                                 SF_D <= CMD_DATA(3 downto 0);
      end case;
   end process;
   
   CLKP : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if RESET = '1' then
            STATE <= WAIT_CMD;
         else
            STATE <= NEXT_STATE;
         end if;
      end if;
   end process;

end LCD_FUNCTION_STM_BODY;