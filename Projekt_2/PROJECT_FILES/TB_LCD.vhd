library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TB_LCD is
end;

architecture TEST of TB_LCD is
component LCD is
   port (
--      EN       : in  std_logic;
      ADDR     : in  std_logic_vector (7 downto 0);
      DATA     : in  std_logic_vector (7 downto 0);
      -- LCD connection
      LCD_E    : out std_logic;
      LCD_RS   : out std_logic;
      LCD_RW   : out std_logic;
      SF_D     : out std_logic_vector (11 downto 8);
      CLK      : in  std_logic;                       -- 50 MHz
      RESET    : in  std_logic
   );
end component LCD;

signal ADDR, DATA : std_logic_vector(7 downto 0);
signal LCD_E, LCD_RS, LCD_RW, CLK, RESET : std_logic;
signal SF_D : std_logic_vector(11 downto 8);

signal TB_RUNNING : std_logic := '1';
signal QUIET_TIME, E_QUIET_TIME : integer := 0;

subtype byte is unsigned(7 downto 0);

procedure WAIT_FOR_BUS_WRITE_NIBBLE(QTIME : in integer; RS: out boolean; NIBBLE : out unsigned(3 downto 0)) is
    type arr is array(0 to 6) of integer;
    constant QTIME_NORMAL : arr := (15_000_000, 4_100_000, 1_640_000, 1_600_000, 100_000, 40_000, 1_000);
    constant QTIME_SIM    : arr := (       400,       400,       400,       400,     300,    200,   100);
    variable I : integer;
begin
    if LCD_E /= '1' then
        wait until LCD_E = '1';
    end if;
    
    -- do QTIME shortening for selected delay values
    for I in QTIME_NORMAL'range loop
        if QTIME_NORMAL(I) = QTIME then
            --Uncomment to get more info
            --report "Mapped min QTIME: " & integer'image(QTIME_SIM(I));
            assert E_QUIET_TIME >= QTIME_SIM(I) report "Function execution time" severity failure;
            exit;
        elsif I = QTIME_NORMAL'high then
            assert E_QUIET_TIME >= QTIME report "Function execution time" severity failure;
            exit;
        end if;
    end loop;
    
    assert QUIET_TIME >= 40 report "Setup time" severity failure;
    assert LCD_RW = '0' report "Reading not supported by the LCD API" severity failure;
    RS := LCD_RS = '1';
    NIBBLE := unsigned(SF_D(11 downto 8));
    wait until LCD_E = '0';
    assert E_QUIET_TIME >= 230 report "E pulse width" severity failure;
    wait for 10 ns;
    assert QUIET_TIME >= 40 + 230 + 10 report "Hold time" severity failure;
end;

procedure WAIT_FOR_BUS_WRITE(QTIME : in integer; RS : out boolean; DATA : out byte) is
    variable HIGH_RS, LOW_RS : boolean;
begin
    WAIT_FOR_BUS_WRITE_NIBBLE(QTIME, HIGH_RS, DATA(7 downto 4));
    WAIT_FOR_BUS_WRITE_NIBBLE(1_000, LOW_RS, DATA(3 downto 0));
    assert HIGH_RS = LOW_RS report "RS on both nibbles" severity failure;
    RS := LOW_RS;
end;

-- LCD state
type t_dd_ram is array(0 to 1, 0 to 39) of character;
shared variable DD_RAM : t_dd_ram;
shared variable DD_RAM_ADDR, NEXT_QTIME : integer;
shared variable INC_OR_DEC, FUNCTION_DONE, ENTRY_MODE_DONE, DISPLAY_ONOFF_DONE, CLEAR_DONE: boolean := false;

begin

DUT : LCD port map(ADDR, DATA, LCD_E, LCD_RS, LCD_RW, SF_D, CLK, RESET);

TESTER : process is
    procedure TEST(X: in std_logic_vector(15 downto 0)) is
    begin
        ADDR <= X(15 downto 8);
        DATA <= X(7 downto 0);
        
        -- Adjust this wait time to be slightly longer then 2 refresh cycles of your controller (measure in waveform)
        wait for 53us;
        -- wait for 200us;
        for I in 0 to 7 loop
            if DATA(8-I-1) = '1' then assert DD_RAM(0, I) = '1' severity failure; else assert DD_RAM(0, I) = '0' severity failure; end if;
            if ADDR(8-I-1) = '1' then assert DD_RAM(1, I) = '1' severity failure; else assert DD_RAM(1, I) = '0' severity failure; end if;
        end loop;
    end;
begin
    -- do reset, then assing values to ADDR and DATA, wait until the bus requests stop coming for a while, then check the display state
    RESET <= '1';
    wait until rising_edge(CLK);
    RESET <= '0';
    
    -- Wait for init
    wait for 12us;
        
    TEST(X"0000");
    TEST(X"FFFF");
    TEST(X"AABB");
    TEST(X"DEAD");
    TEST(X"BEEF");
    TEST(X"0000");
    
    TB_RUNNING <= '0';
    report "KONEC SIMULACE" severity warning;
    wait;
end process;

CONTROLLER : process is
    procedure SHIFT_CURSOR(INC_DEC : in boolean) is
    begin
        if INC_DEC then
            if DD_RAM_ADDR /= 16#27# then
                DD_RAM_ADDR := DD_RAM_ADDR + 1;
            else
                DD_RAM_ADDR := 16#40#;
            end if;
        else
            DD_RAM_ADDR := DD_RAM_ADDR - 1;
        end if;
    end;

    variable NIBBLE : unsigned(3 downto 0);
    variable DATA : byte;
    variable RS : boolean;
    variable I: integer;
begin
    -- wait for bootup time
    -- first wait on init sequence, incl. Function Set, Entry Mode Set, Display On/Off, Clear Display
    -- then infinite loop, crasing on non-supported funtions (eg: display off, custom characters, shifting, blinking cursor)
    WAIT_FOR_BUS_WRITE_NIBBLE(15_000_000, RS, NIBBLE);
    assert RS = false and NIBBLE = X"3" report "Init 1" severity failure;
    WAIT_FOR_BUS_WRITE_NIBBLE(4_100_000, RS, NIBBLE);
    assert RS = false and NIBBLE = X"3" report "Init 2" severity failure;
    WAIT_FOR_BUS_WRITE_NIBBLE(100_000, RS, NIBBLE);
    assert RS = false and NIBBLE = X"3" report "Init 3" severity failure;
    WAIT_FOR_BUS_WRITE_NIBBLE(40_000, RS, NIBBLE);
    assert RS = false and NIBBLE = X"2" report "Init 4" severity failure;
    NEXT_QTIME := 40_000;
    
    -- infinite loop with asserts that DD_RAM_ADDR manipulation or DD_RAM writes only happen after initialization commands are done (in the correct order)
    while true loop
        WAIT_FOR_BUS_WRITE(NEXT_QTIME, RS, DATA);
        NEXT_QTIME := 40_000; -- Most common value
        if not RS then
            if DATA = B"0000_0001" then
                -- Clear Display
                assert DISPLAY_ONOFF_DONE severity failure;
                for I in DD_RAM'range(1) loop
                    DD_RAM(0, I) := ' '; DD_RAM(1, I) := ' ';
                end loop;
                DD_RAM_ADDR := 0;
                CLEAR_DONE := true;
                NEXT_QTIME := 1_640_000;
            elsif (DATA and B"1111_1110") = B"0000_0010" then
                -- Return Cursor home
                DD_RAM_ADDR := 0;
                NEXT_QTIME := 1_600_000;
            elsif (DATA and B"1111_1100") = B"0000_0100" then
                -- Entry Mode Set
                assert FUNCTION_DONE severity failure;
                INC_OR_DEC := DATA(1) = '1';
                assert DATA(0) = '0' severity failure; -- Display shift unsupported
                ENTRY_MODE_DONE := true;
            elsif (DATA and B"1111_1000") = B"0000_1000" then
                -- Display On/Off
                assert ENTRY_MODE_DONE severity failure;
                assert DATA(2 downto 0) = B"100" severity failure; -- Display characeters, no cursor, no blink
                DISPLAY_ONOFF_DONE := true;
            elsif (DATA and B"1111_0000") = B"0001_0000" then
                -- Cursor and Display Shift
                assert DATA(3) = '0' severity failure; -- Shift unsupported
                SHIFT_CURSOR(DATA(2) = '1');
            elsif (DATA and B"1110_0000") = B"0010_0000" then
                -- Function Set
                assert DATA = 16#28# severity failure; -- Only function set 0x28 supported
                FUNCTION_DONE := true;
            elsif (DATA and B"1100_0000") = B"0100_0000" then
                -- Set CG RAM Address
                assert false severity failure; -- Custom characters unsupported
            elsif (DATA and B"1000_0000") = B"1000_0000" then
                -- Set DD RAM Address
                DD_RAM_ADDR := to_integer(data(6 downto 0));
            else
                assert false severity failure;
            end if;
        else
            -- Write Data to DD RAM
            assert ENTRY_MODE_DONE and DISPLAY_ONOFF_DONE and CLEAR_DONE severity failure;
            assert (0 <= DD_RAM_ADDR and DD_RAM_ADDR <= 16#27#) or (16#40# <= DD_RAM_ADDR and DD_RAM_ADDR <= 16#67#) severity failure;
            DD_RAM(DD_RAM_ADDR / 64, DD_RAM_ADDR mod 64) := character'val(to_integer(DATA));
            SHIFT_CURSOR(INC_OR_DEC);
        end if;
    end loop;
end process;

CLOCKER : process is
begin
    CLK <= '0';
    wait for 10 ns;
    CLK <= '1';
    wait for 10 ns;
    if TB_RUNNING = '0' then wait; end if;
end process;

QUIET_TRACKER : process (CLK, LCD_E, LCD_RS, LCD_RW, SF_D) is
begin
    if CLK'event then
        QUIET_TIME <= QUIET_TIME + 10 after 1 ns;
        E_QUIET_TIME <= E_QUIET_TIME + 10 after 1 ns;
    end if;
    if LCD_RS'event or LCD_RW'event or SF_D'event then
        QUIET_TIME <= 10 after 1 ns;
    end if;
    if LCD_E'event then
        E_QUIET_TIME <= 10 after 1 ns;
    end if;
end process;

end;