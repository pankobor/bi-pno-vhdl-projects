library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity DEBOUNCE_RE_DETECT is
  generic (
    REG_WIDTH : integer := 20
  );
  port(
    X, CLK, RESET : in std_logic;
    Y : out std_logic
  );
end DEBOUNCE_RE_DETECT;

architecture ARCH of DEBOUNCE_RE_DETECT is
  signal CTR : unsigned(REG_WIDTH - 1 downto  0);
  signal PX : std_logic;
begin

Y <= X when PX /= X and CTR = 0 else '0';

process(CLK) begin
  if rising_edge(CLK) then
    PX <= X;
    if RESET = '1' then
      CTR <= (others => '0');
    else
      if PX /= X then
        CTR <= (others => '1');
      elsif not (CTR = 0) then
        CTR <= CTR - 1;
      end if;
    end if;
  end if;
end process;
end architecture ARCH;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ROTARY is
  generic (
    DEBOUNCE_REG_WIDTH : integer := 20
  );
  port (
      ROTARY   : in  std_logic_vector (1 downto 0);  -- rotary button  (left, right)
  --    EN       : out  std_logic;
      ADDR     : out std_logic_vector (7 downto 0);  -- address register
      COARSE   : in  std_logic;                      -- fine or coarse incrementation (rotary button push)
      CLK      : in  std_logic;                      -- 50 MHz
      RESET    : in  std_logic
  );
end ROTARY;

architecture ARCH of ROTARY is
  signal STEP, DIR, Y, COARSE_EDGE, BIGSTEP : std_logic;
  signal DADDR, INTERNAL_ADDR : signed(7 downto 0);
begin

DEBX      : entity work.DEBOUNCE_RE_DETECT generic map(REG_WIDTH => DEBOUNCE_REG_WIDTH) port map(X => ROTARY(0), Y => STEP,      CLK => CLK, RESET => RESET);
DEBCOARSE : entity work.DEBOUNCE_RE_DETECT generic map(REG_WIDTH => DEBOUNCE_REG_WIDTH) port map(X => COARSE,    Y => COARSE_EDGE, CLK => CLK, RESET => RESET);

DIR <= ROTARY(1); -- Doesn't need to be filtered since we don't detect edges on this signal

with std_logic_vector'(DIR & BIGSTEP) select DADDR <=
    to_signed(  1, 8) when "00",
    to_signed( -1, 8) when "10",
    to_signed(  8, 8) when "01",
    to_signed( -8, 8) when "11";

ADDR <= std_logic_vector(INTERNAL_ADDR);

process (CLK) begin
  if rising_edge(CLK) then
    if RESET = '1' then
        INTERNAL_ADDR <= (others => '0');
        BIGSTEP <= '0';
    else
      if COARSE_EDGE='1' then
        BIGSTEP <= not BIGSTEP;
      end if;
      if STEP = '1' then
        INTERNAL_ADDR <= INTERNAL_ADDR + DADDR;
      end if;
    end if;
  end if;
end process;
end architecture ARCH;
