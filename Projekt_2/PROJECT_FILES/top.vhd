----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Top level entity for integration of LCD and rotary button
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TOP is
   port (
      ROTARY_IN: in  std_logic_vector (1 downto 0);
      COARSE   : in  std_logic;
      CLK      : in  std_logic;
      RESET    : in  std_logic;
      SF_D     : out std_logic_vector (11 downto 8);
      LCD_E    : out std_logic;
      LCD_RS   : out std_logic;
      LCD_RW   : out std_logic;
		OUT_LED : out std_logic_vector (1 downto 0);
		OUT_LED_C : out std_logic
   );
end TOP;

architecture TOP_BODY of TOP is
   
   component LCD is
      port (
         ADDR     : in  std_logic_vector (7 downto 0);
         DATA     : in  std_logic_vector (7 downto 0);
         -- LCD connection
         LCD_E    : out std_logic;
         LCD_RS   : out std_logic;
         LCD_RW   : out std_logic;
         SF_D     : out std_logic_vector (11 downto 8);
         CLK      : in  std_logic;                       -- 50 MHz
         RESET    : in  std_logic
      );
   end component LCD;
   
   component ROTARY is
		generic (
		 DEBOUNCE_REG_WIDTH : integer := 20
	  );
	  port (
         ROTARY   : in  std_logic_vector (1 downto 0);  -- rotary button  (left, right)
         ADDR     : out std_logic_vector (7 downto 0);  -- address register
         COARSE   : in  std_logic;                      -- fine or coarse incrementation (rotary button push)
         CLK      : in  std_logic;                      -- 50 MHz
         RESET    : in  std_logic
     );
   end component ROTARY;
   
   component ROM is
      port (
         CLK  : in  std_logic;
         ADDR : in  std_logic_vector(7 downto 0);
         DATA : out std_logic_vector(7 downto 0)
      );
   end component ROM;
   
   signal ADDR : std_logic_vector (7 downto 0);
   signal DATA : std_logic_vector (7 downto 0);
   
begin

	OUT_LED <= ROTARY_IN;
	OUT_LED_C <= COARSE;

   LCD_INST : LCD port map (
      ADDR     => ADDR,
      DATA     => DATA,
      LCD_E    => LCD_E,
      LCD_RS   => LCD_RS,
      LCD_RW   => LCD_RW,
      SF_D     => SF_D,
      CLK      => CLK,
      RESET    => RESET
   );
   
   ROTARY_INST : ROTARY generic map(20)
		port map (
      ROTARY   => not ROTARY_IN,
      ADDR     => ADDR,
      COARSE   => COARSE,
      CLK      => CLK,
      RESET    => RESET
   );
   
   ROM_INST : ROM port map (
      CLK      => CLK,
      ADDR     => ADDR,
      DATA     => DATA
   );


end TOP_BODY;
