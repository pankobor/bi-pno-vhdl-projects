----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Testbench for rotary switch
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TB_INTEGRAL_ROTARY is
end TB_INTEGRAL_ROTARY;

architecture TB_INTEGRAL_ROTARY_BODY of TB_INTEGRAL_ROTARY is

   component ROTARY is
      port (
         ROTARY   : in  std_logic_vector (1 downto 0);
         ADDR     : out std_logic_vector (7 downto 0);
         COARSE   : in  std_logic;
         CLK      : in  std_logic;
         RESET    : in  std_logic
      );
   end component ROTARY;
   
   signal TB_ROTARY    : std_logic_vector (1 downto 0);
   signal TB_ADDR      : std_logic_vector (7 downto 0);
   signal TB_COARSE    : std_logic;
   signal TB_CLK       : std_logic;
   signal TB_RESET     : std_logic;
   
   constant CLK_PERIOD : time := 20 ns; --50 Mhz
   
   signal A   : unsigned (7 downto 0) := (others => '0');
   signal C   : std_logic := '0';
   
   procedure ROTATE_R ( signal A      : inout unsigned;
                        signal C      : in std_logic;
                        signal ROTARY : out std_logic_vector (1 downto 0)) is
   begin
      ROTARY <= "01";
      wait for 8 * CLK_PERIOD;
      ROTARY <= "11";
      wait for 8 * CLK_PERIOD;
      ROTARY <= "10";
      wait for 8 * CLK_PERIOD;
      ROTARY <= "00";
      if C = '0' then
         A <= A + 1;
      else 
         A <= A + 8;
      end if;
      wait for 8 * CLK_PERIOD;
   end ROTATE_R;
      
   procedure ROTATE_L ( signal A      : inout unsigned;
                        signal C      : in std_logic;
                        signal ROTARY : out std_logic_vector (1 downto 0)) is
   begin
      ROTARY <= "10";
      wait for 8 * CLK_PERIOD;
      ROTARY <= "11";
      wait for 8 * CLK_PERIOD;
      ROTARY <= "01";
      wait for 8 * CLK_PERIOD;
      ROTARY <= "00";
      if C = '0' then
         A <= A - 1;
      else 
         A <= A - 8;
      end if;
      wait for 8 * CLK_PERIOD;
   end ROTATE_L;
   
   procedure DO_ASSERT (signal A    : in unsigned;
                        signal ADDR : in std_logic_vector (7 downto 0)) is
   begin
      assert ADDR = STD_LOGIC_VECTOR(A)
            report "ERROR: Output of DUT: " & integer'image(TO_INTEGER(UNSIGNED(ADDR))) & "; Expected output: " & integer'image(TO_INTEGER(A)) 
            severity error;
   end DO_ASSERT;

begin

   DUT : ROTARY port map (
      ROTARY   => TB_ROTARY,
      ADDR     => TB_ADDR,
      COARSE   => TB_COARSE,
      CLK      => TB_CLK,
      RESET    => TB_RESET
   );
   
   CLK_GEN : process
   begin
      TB_CLK <= '0';
      wait for CLK_PERIOD / 2;
      TB_CLK <= '1';
      wait for CLK_PERIOD / 2;
   end process;
   
   RESET_GEN : process
   begin
      wait for 2 * CLK_PERIOD;
      TB_RESET <= '0';
      wait for 2 * CLK_PERIOD;
      TB_RESET <= '1';
      wait for 2 * CLK_PERIOD;
      TB_RESET <= '0';
      wait;
   end process;
   
   STIMULI_GEN : process
   begin
      wait until TB_RESET = '1';
      TB_ROTARY <= "00";
      wait until TB_RESET = '0';
      wait for 2 * CLK_PERIOD;
      
      DO_ASSERT(A, TB_ADDR); --check reset of addr

------SOFT----------------------------------------------------------------------------------------------
      ROTATE_R(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after one right rotation   
      for I in 0 to 31 loop
         ROTATE_R(A, C, TB_ROTARY);
      end loop;
      DO_ASSERT(A, TB_ADDR); --check addr after multiple right rotations
      
      ROTATE_L(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after one left rotation     
      for I in 0 to 15 loop
         ROTATE_L(A, C, TB_ROTARY);
      end loop;
      DO_ASSERT(A, TB_ADDR); --check addr after multiple left rotations
      
      for I in 0 to 15 loop
         ROTATE_R(A, C, TB_ROTARY);
         ROTATE_L(A, C, TB_ROTARY);
      end loop;
      DO_ASSERT(A, TB_ADDR); --check addr after multiple right-left rotations
      
      for I in 0 to 15 loop
         ROTATE_L(A, C, TB_ROTARY);
      end loop;
      DO_ASSERT(A, TB_ADDR); --rotate to 0 and check
      
      ROTATE_L(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after underflow
      
      ROTATE_R(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after overflow
      
------COARSE--------------------------------------------------------------------------------------      
      C <= '1';
      TB_COARSE <= '1';
      wait for 5 * CLK_PERIOD;
      TB_COARSE <= '0';
      
      DO_ASSERT(A, TB_ADDR); --check addr after coarse click
      
      ROTATE_R(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after one right rotation   
      for I in 0 to 29 loop
         ROTATE_R(A, C, TB_ROTARY);
      end loop;
      DO_ASSERT(A, TB_ADDR); --check addr after multiple right rotations
      
      ROTATE_R(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after overflow
      
      ROTATE_L(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after underflow
      
      ROTATE_L(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after one left rotation     
      for I in 0 to 15 loop
         ROTATE_L(A, C, TB_ROTARY);
      end loop;
      DO_ASSERT(A, TB_ADDR); --check addr after multiple left rotations
      
      for I in 0 to 15 loop
         ROTATE_R(A, C, TB_ROTARY);
         ROTATE_L(A, C, TB_ROTARY);
      end loop;
      DO_ASSERT(A, TB_ADDR); --check addr after multiple right-left rotations

------ROTARY-NOISE---------------------------------------------------------------------------------
      -- TB_ROTARY 00 -> 01 with noise
      TB_ROTARY <= "01";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "00";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "01";
      wait for 3 * CLK_PERIOD;
      TB_ROTARY <= "00";
      wait for CLK_PERIOD;
      TB_ROTARY <= "01";
      
      -- TB_ROTARY 01 -> 11 with noise
      TB_ROTARY <= "11";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "01";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "11";
      wait for 3 * CLK_PERIOD;
      TB_ROTARY <= "01";
      wait for CLK_PERIOD;
      TB_ROTARY <= "11";
      
      -- TB_ROTARY 11 -> 10 with noise
      TB_ROTARY <= "10";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "11";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "10";
      wait for 3 * CLK_PERIOD;
      TB_ROTARY <= "11";
      wait for CLK_PERIOD;
      TB_ROTARY <= "10";
      
      -- TB_ROTARY 10 -> 00 with noise
      TB_ROTARY <= "00";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "10";
      wait for 2 * CLK_PERIOD;
      TB_ROTARY <= "00";
      wait for 3 * CLK_PERIOD;
      TB_ROTARY <= "10";
      wait for CLK_PERIOD;
      TB_ROTARY <= "00";
      
      A <= A + 8;
      wait for 5 * CLK_PERIOD;
      
      DO_ASSERT(A, TB_ADDR);
      
------SWITCH-NOISE---------------------------------------------------------------------------------
      C <= '0';
      
      -- TB_COARSE 0 -> 1 with noise == pushing button down
      TB_COARSE <= '1';
      wait for 2 * CLK_PERIOD;
      TB_COARSE <= '0';
      wait for 2 * CLK_PERIOD;
      TB_COARSE <= '1';
      wait for 3 * CLK_PERIOD;
      TB_COARSE <= '0';
      wait for CLK_PERIOD;
      TB_COARSE <= '1';      
      wait for 5 * CLK_PERIOD;
      
      -- TB_COARSE 1 -> 0 with noise == releasing button
      TB_COARSE <= '0';
      wait for 2 * CLK_PERIOD;
      TB_COARSE <= '1';
      wait for 2 * CLK_PERIOD;
      TB_COARSE <= '0';
      wait for 3 * CLK_PERIOD;
      TB_COARSE <= '1';
      wait for CLK_PERIOD;
      TB_COARSE <= '0';      
      wait for 5 * CLK_PERIOD;
      
      DO_ASSERT(A, TB_ADDR); --check addr after coarse click, should be same
      
      ROTATE_R(A, C, TB_ROTARY);
      DO_ASSERT(A, TB_ADDR); --check addr after one right rotation, to check if corase click with noise was succesfull
      
      report "END OF SIMULATION" severity failure;
   end process;
end TB_INTEGRAL_ROTARY_BODY;
