----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Controller of LCD driver
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity CONTROLLER is
   port (
      DATA        : in  std_logic_vector (7 downto 0);
      ADDR        : in  std_logic_vector (7 downto 0);
      CMD_DONE    : in  std_logic;
      CLK         : in  std_logic;
      RESET       : in  std_logic;
      CMD_START   : out std_logic;
      INIT_START  : out std_logic; 
      LCD_RS      : out std_logic;
      CMD_DATA    : out std_logic_vector (7 downto 0)
   );
end CONTROLLER;

architecture CONTROLLER_BODY of CONTROLLER is

   signal COPY_ADDR : std_logic_vector(7 downto 0);
   signal COPY_DATA : std_logic_vector(7 downto 0);
   
   signal CNT     : unsigned (29 downto 0);
   signal CNT1    : unsigned (29 downto 0);
   signal EN_CNT : std_logic;
   signal LD_CNT : std_logic;
   signal ZERO   : std_logic;
   signal SAVE_ADDR  : std_logic;
   signal SAVE_DATA  : std_logic;
   
   type TYPE_STATE is (I1, I2, I3, I4, I5, I6, I7, I8,
                       FUNCTION_SET, ENTRY_MODE_SET, DISP_ON, CLEAR_DISPLAY, CLR_DISP_WAIT, SET_ADDR_A, SET_ADDR_D, 
                       A0, A1, A2, A3, A4, A5, A6, A7, D0, D1, D2, D3, D4, D5, D6, D7, WAIT_CHANGE);
   signal STATE, NEXT_STATE : TYPE_STATE;
   
begin

   AUX_COUNTER1 : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if LD_CNT = '1' then              
            CNT1 <= CNT;
         elsif EN_CNT = '1' then           
            CNT1 <= CNT1 - 1;
         end if;
      end if;
   end process;
   
   CNT1_FINISH : process (CNT1)
      begin
         if CNT1 = 0 then
            ZERO <= '1';
         else
            ZERO <= '0';
         end if;
   end process;
   
   REG_ADDR : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if RESET = '1' then
            COPY_ADDR <= "00000000";
         elsif SAVE_ADDR = '1' then
            COPY_ADDR <= ADDR;
         end if;
      end if;
   end process;
   
   REG_DATA : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if RESET = '1' then
            COPY_DATA <= "00000000";
         elsif SAVE_DATA = '1' then
            COPY_DATA <= DATA;
         end if;
      end if;
   end process;
   
   TRANP : process (STATE, CMD_DONE, ZERO, DATA, ADDR, COPY_DATA, COPY_ADDR)
   begin
      case STATE is
         when I1              => NEXT_STATE <= I2; 
         when I2              => if ZERO = '1' then
                                    NEXT_STATE <= I3;
                                 else
                                    NEXT_STATE <= I2;
                                 end if;
         when I3              => if CMD_DONE = '1' then
                                    NEXT_STATE <= I4;
                                 else
                                    NEXT_STATE <= I3;
                                 end if;
         when I4              => if ZERO = '1' then
                                    NEXT_STATE <= I5;
                                 else
                                    NEXT_STATE <= I4;
                                 end if;
         when I5              => if CMD_DONE = '1' then
                                    NEXT_STATE <= I6;
                                 else
                                    NEXT_STATE <= I5;
                                 end if;
         when I6             => if ZERO = '1' then
                                    NEXT_STATE <= I7;
                                 else
                                    NEXT_STATE <= I6;
                                 end if;
         when I7             => if CMD_DONE = '1' then
                                    NEXT_STATE <= I8;
                                 else
                                    NEXT_STATE <= I7;
                                 end if;
         when I8             => if CMD_DONE = '1' then
                                    NEXT_STATE <= FUNCTION_SET;
                                 else
                                    NEXT_STATE <= I8;
                                 end if;
         when FUNCTION_SET    => if CMD_DONE = '1' then
                                    NEXT_STATE <= ENTRY_MODE_SET;
                                 else
                                    NEXT_STATE <= FUNCTION_SET;
                                 end if;
         when ENTRY_MODE_SET  => if CMD_DONE = '1' then
                                    NEXT_STATE <= DISP_ON;
                                 else
                                    NEXT_STATE <= ENTRY_MODE_SET;
                                 end if;
         when DISP_ON         => if CMD_DONE = '1' then
                                    NEXT_STATE <= CLEAR_DISPLAY;
                                 else
                                    NEXT_STATE <= DISP_ON;
                                 end if;
         when CLEAR_DISPLAY   => if CMD_DONE = '1' then
                                    NEXT_STATE <= CLR_DISP_WAIT;
                                 else
                                    NEXT_STATE <= CLEAR_DISPLAY;
                                 end if;
         when CLR_DISP_WAIT   => if ZERO = '1' then
                                    NEXT_STATE <= SET_ADDR_A;
                                 else
                                    NEXT_STATE <= CLR_DISP_WAIT;
                                 end if;
         when SET_ADDR_A      => if CMD_DONE = '1' then
                                    NEXT_STATE <= A7;
                                 else
                                    NEXT_STATE <= SET_ADDR_A;
                                 end if;
         when A7              => if CMD_DONE = '1' then
                                    NEXT_STATE <= A6;
                                 else
                                    NEXT_STATE <= A7;
                                 end if;
         when A6              => if CMD_DONE = '1' then
                                    NEXT_STATE <= A5;
                                 else
                                    NEXT_STATE <= A6;
                                 end if;
         when A5              => if CMD_DONE = '1' then
                                    NEXT_STATE <= A4;
                                 else
                                    NEXT_STATE <= A5;
                                 end if;
         when A4              => if CMD_DONE = '1' then
                                    NEXT_STATE <= A3;
                                 else
                                    NEXT_STATE <= A4;
                                 end if;
         when A3              => if CMD_DONE = '1' then
                                    NEXT_STATE <= A2;
                                 else
                                    NEXT_STATE <= A3;
                                 end if;
         when A2              => if CMD_DONE = '1' then
                                    NEXT_STATE <= A1;
                                 else
                                    NEXT_STATE <= A2;
                                 end if;
         when A1              => if CMD_DONE = '1' then
                                    NEXT_STATE <= A0;
                                 else
                                    NEXT_STATE <= A1;
                                 end if;
         when A0              => if CMD_DONE = '1' then
                                    NEXT_STATE <= SET_ADDR_D;
                                 else
                                    NEXT_STATE <= A0;
                                 end if;
         when SET_ADDR_D      => if CMD_DONE = '1' then
                                    NEXT_STATE <= D7;
                                 else
                                    NEXT_STATE <= SET_ADDR_D;
                                 end if;
         when D7              => if CMD_DONE = '1' then
                                    NEXT_STATE <= D6;
                                 else
                                    NEXT_STATE <= D7;
                                 end if;
         when D6              => if CMD_DONE = '1' then
                                    NEXT_STATE <= D5;
                                 else
                                    NEXT_STATE <= D6;
                                 end if;
         when D5              => if CMD_DONE = '1' then
                                    NEXT_STATE <= D4;
                                 else
                                    NEXT_STATE <= D5;
                                 end if;
         when D4              => if CMD_DONE = '1' then
                                    NEXT_STATE <= D3;
                                 else
                                    NEXT_STATE <= D4;
                                 end if;
         when D3              => if CMD_DONE = '1' then
                                    NEXT_STATE <= D2;
                                 else
                                    NEXT_STATE <= D3;
                                 end if;
         when D2              => if CMD_DONE = '1' then
                                    NEXT_STATE <= D1;
                                 else
                                    NEXT_STATE <= D2;
                                 end if;
         when D1              => if CMD_DONE = '1' then
                                    NEXT_STATE <= D0;
                                 else
                                    NEXT_STATE <= D1;
                                 end if;
         when D0              => if CMD_DONE = '1' then
                                    NEXT_STATE <= WAIT_CHANGE; --SET_ADDR_A;
                                 else
                                    NEXT_STATE <= D0;
                                 end if;
         when WAIT_CHANGE     => if DATA = COPY_DATA and ADDR = COPY_ADDR then
                                    NEXT_STATE <= WAIT_CHANGE;
                                 else
                                    NEXT_STATE <= SET_ADDR_A;
                                 end if;
      end case;
   end process;
   
   OUTP : process (STATE)
   begin
      CMD_START   <= '0';
      INIT_START  <= '0'; 
      LCD_RS      <= '0';
      CMD_DATA    <= (others => '0');
      EN_CNT      <= '0';
      LD_CNT      <= '0';
      CNT         <= to_unsigned(0, 30);
      SAVE_ADDR   <= '0';
      SAVE_DATA   <= '0';
      case STATE is
         when I1              => LD_CNT <= '1'; --loading counter for initial wait
                                 CNT <= to_unsigned(750000, 30); --750000
         when I2              => EN_CNT <= '1'; -- waiting for 15ms
         when I3              => LD_CNT <= '1'; -- loading counter for another wait
                                 CNT <= to_unsigned(205000, 30); --203000 + 2000
                                 INIT_START <= '1'; -- 1st init. data transfer
                                 CMD_DATA <= "00000011";
         when I4              => EN_CNT <= '1'; -- waiting for 4100us
         when I5              => LD_CNT <= '1'; 
                                 CNT <= to_unsigned(5000, 30); --3000 + 2000
                                 INIT_START <= '1'; -- 2nd init. data transfer
                                 CMD_DATA <= "00000011";
         when I6              => EN_CNT <= '1'; -- waiting for 100us
         when I7              => INIT_START <= '1'; -- no counter needed, waiting for 40us will handle the function stm
                                 CMD_DATA <= "00000011"; -- 3rd init. data transfer
         when I8              => INIT_START <= '1';
                                 CMD_DATA <= "00000010"; -- 4rd and final init. data transfer
         when FUNCTION_SET    => CMD_DATA <= "00101000"; -- now LCD is ready to communicate
                                 CMD_START <= '1';       -- so now we are sending functions to set up lcd interface
         when ENTRY_MODE_SET  => CMD_DATA <= "00000110";
                                 CMD_START <= '1';
         when DISP_ON         => CMD_DATA <= "00001100";
                                 CMD_START <= '1';
         when CLEAR_DISPLAY   => CMD_DATA <= "00000001";
                                 CMD_START <= '1';
                                 LD_CNT <= '1'; -- loading counter for wait after clearing display
                                 CNT <= to_unsigned(82000, 30); --80000 + 2000
         when CLR_DISP_WAIT   => EN_CNT <= '1'; -- waiting for 1.64ms
         when SET_ADDR_A      => CMD_DATA <= "11000000"; -- addr 0x40 = starting addr of second row
                                 CMD_START <= '1';
                                 SAVE_ADDR <= '1'; -- save inputs so if they change while sending data
                                 SAVE_DATA <= '1'; -- they won't mess up on display
         when A7              => LCD_RS <= '1'; -- for 8 times send a 0 or 1 on display
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(7); -- address of LCD DD ram = either 0 or 1 character
         when A6              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(6);
         when A5              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(5);
         when A4              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(4);
         when A3              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(3);
         when A2              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(2);
         when A1              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(1);
         when A0              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_ADDR(0);
         when SET_ADDR_D      => CMD_DATA <= "10000000"; -- addr 0x00 = starting addr of first row
                                 CMD_START <= '1';
         when D7              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(7);
         when D6              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(6);
         when D5              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(5);
         when D4              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(4);
         when D3              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(3);
         when D2              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(2);
         when D1              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(1);
         when D0              => LCD_RS <= '1';
                                 CMD_START <= '1';
                                 CMD_DATA <= "0011000" & COPY_DATA(0);
         when WAIT_CHANGE     => 
      end case;
      
   end process;
   
   CLKP : process (CLK)
   begin
      if CLK = '1' and CLK'event then
         if RESET = '1' then
            STATE <= I1; --I1
         else
            STATE <= NEXT_STATE;
         end if;
      end if;
   end process;


end CONTROLLER_BODY;
