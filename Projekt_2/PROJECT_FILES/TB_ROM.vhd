----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Testbench for ROM simulating entity
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity TB_ROM is
end TB_ROM;

architecture TB_ROM_BODY of TB_ROM is
   component ROM is
      port (
         CLK  : in  std_logic;
         ADDR : in  std_logic_vector(7 downto 0);
         DATA : out std_logic_vector(7 downto 0)
      );
   end component ROM;
   
   signal ADDR : std_logic_vector(7 downto 0);
   signal DATA : std_logic_vector(7 downto 0);
   signal CLK : std_logic;
   
begin

   CLK_GEN : process 
   begin
      CLK <= '1';
      wait for 10 ns;
      CLK <= '0';
      wait for 10 ns;
   end process;

   DUT : ROM port map (
      CLK   => CLK,
      ADDR  => ADDR,
      DATA  => DATA
   );
   
   TESTER : process
   begin
      wait for 5 ns;
      ADDR <= "00000000";
      wait for 20 ns;
      ADDR <= "11111111";
      wait for 20 ns;
      ADDR <= "11110000";
      wait for 20 ns;
      ADDR <= "10101010";
      wait for 20 ns;
   end process;

end TB_ROM_BODY;
