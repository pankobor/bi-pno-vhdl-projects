----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Top entity for LCD only presentation
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity LCD_AND_IO is
   port (
      ADDR_UP  : in  std_logic;
      ADDR_DOWN: in  std_logic;
      DATA_UP  : in  std_logic;
      DATA_DOWN: in  std_logic;
      LCD_E    : out std_logic;
      LCD_RS   : out std_logic;
      LCD_RW   : out std_logic;
      SF_D     : out std_logic_vector (11 downto 8);
      CLK      : in  std_logic;                       -- 50 MHz
      RESET    : in  std_logic
   );
end LCD_AND_IO;

architecture LCD_AND_IO_BODY of LCD_AND_IO is

   component LCD is
      port (
         ADDR     : in  std_logic_vector (7 downto 0);
         DATA     : in  std_logic_vector (7 downto 0);
         -- LCD connection
         LCD_E    : out std_logic;
         LCD_RS   : out std_logic;
         LCD_RW   : out std_logic;
         SF_D     : out std_logic_vector (11 downto 8);
         CLK      : in  std_logic;                       -- 50 MHz
         RESET    : in  std_logic
      );
   end component LCD;

   signal DATA_VAL : unsigned(7 downto 0);
   signal ADDR_VAL : unsigned(7 downto 0);
   
   signal DATA : std_logic_vector(7 downto 0);
   signal ADDR : std_logic_vector(7 downto 0);
      
begin

   DATA_P : process (CLK)
   begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				DATA_VAL <= to_unsigned(0, 8);
			elsif DATA_UP = '1' then
				DATA_VAL <= DATA_VAL + 1;
			elsif DATA_DOWN = '1' then
				DATA_VAL <= DATA_VAL - 1;
			end if;
			DATA <= std_logic_vector(DATA_VAL);
		end if;
   end process;
   
   ADDR_P : process (CLK)
   begin
		if CLK = '1' and CLK'event then
			if RESET = '1' then
				ADDR_VAL <= to_unsigned(0, 8);
			elsif ADDR_UP = '1' then
				ADDR_VAL <= ADDR_VAL + 1;
			elsif ADDR_DOWN = '1' then
				ADDR_VAL <= ADDR_VAL - 1;
			end if;
			ADDR <= std_logic_vector(ADDR_VAL);
		end if;
   end process;
   
   LCD_INST : LCD port map(
      ADDR     => ADDR,
      DATA     => DATA,
      LCD_E    => LCD_E,
      LCD_RS   => LCD_RS,
      LCD_RW   => LCD_RW,
      SF_D     => SF_D,
      CLK      => CLK,
      RESET    => RESET
   );

end LCD_AND_IO_BODY;
