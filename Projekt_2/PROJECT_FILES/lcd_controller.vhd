----------------------------------------------------------------------------------
-- Company:     CTU in Prague, Faculty of Information Technology
-- Engineer:    Boris Pankovcin
--  
-- Description: Entity driving LCD display
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity LCD is
   port (
      ADDR     : in  std_logic_vector (7 downto 0);
      DATA     : in  std_logic_vector (7 downto 0);
      -- LCD connection
      LCD_E    : out std_logic;
      LCD_RS   : out std_logic;
      LCD_RW   : out std_logic;
      SF_D     : out std_logic_vector (11 downto 8);
      CLK      : in  std_logic;                       -- 50 MHz
      RESET    : in  std_logic
   );
end LCD;

architecture LCD_BODY of LCD is
   component LCD_FUNCTION_STM is
      port (
         CMD_DATA    : in  std_logic_vector (7 downto 0);
         CMD_START   : in  std_logic;
         INIT_START  : in  std_logic;
         CLK         : in  std_logic;
         RESET       : in  std_logic;
         SF_D        : out std_logic_vector (11 downto 8);
         CMD_DONE    : out std_logic;
         LCD_E       : out std_logic
      );
   end component LCD_FUNCTION_STM;
   
   component CONTROLLER is
      port (
         DATA        : in  std_logic_vector (7 downto 0);
         ADDR        : in  std_logic_vector (7 downto 0);
         CMD_DONE    : in  std_logic;
         CLK         : in  std_logic;
         RESET       : in  std_logic;
         CMD_START   : out std_logic;
         INIT_START  : out std_logic; 
         LCD_RS      : out std_logic;
         CMD_DATA    : out std_logic_vector (7 downto 0)
      );
   end component CONTROLLER;
   
   signal CMD_START  : std_logic;
   signal CMD_DONE   : std_logic;
   signal INIT_START : std_logic;
   signal CMD_DATA   : std_logic_vector (7 downto 0);
   
begin

   LCD_FUNCTION_STM_INST : LCD_FUNCTION_STM port map (
      CMD_DATA    => CMD_DATA,
      CMD_START   => CMD_START,
      INIT_START  => INIT_START,
      CLK         => CLK,
      RESET       => RESET,
      SF_D        => SF_D,
      LCD_E       => LCD_E,
      CMD_DONE    => CMD_DONE
   );
   
   CONTROLLER_INST : CONTROLLER port map (
      DATA        => DATA,
      ADDR        => ADDR,
      CMD_DONE    => CMD_DONE,
      CLK         => CLK,
      RESET       => RESET,
      CMD_START   => CMD_START,
      INIT_START  => INIT_START, 
      LCD_RS      => LCD_RS,
      CMD_DATA    => CMD_DATA
   );
   
   LCD_RW <= '0';

end LCD_BODY;